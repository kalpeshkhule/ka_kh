package userRepository

import (
	"context"
	"database/sql"
	"log"
)

func (u UserRepository) AddOtp(ctx context.Context, otp string, userID int) error {
	statement, err := u.db.Prepare("INSERT INTO `otp` (`otp`, `user_id`) VALUES (?, ?)")
	if err != nil {
		log.Println(err)
		return err
	}
	defer statement.Close()

	_, err = statement.Exec(otp, userID)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (u UserRepository) ValidateOtp(ctx context.Context, userID int, otp string) error {
	statement, err := u.db.Prepare("select * from otp where created_at >= date_sub(now(), interval 15 minute) AND user_id=? AND otp=?")
	if err != nil {
		log.Println(err)
		return err
	}
	defer statement.Close()

	rows, err := statement.Query(userID, otp)
	if err != nil {
		log.Println(err)
		return err
	}
	defer rows.Close()

	if !rows.Next() {
		return sql.ErrNoRows
	}

	return nil
}
