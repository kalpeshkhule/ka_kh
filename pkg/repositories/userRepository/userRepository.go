package userRepository

import "database/sql"

type UserRepository struct {
	db *sql.DB
}

func GetUserRepository(db *sql.DB) *UserRepository {
	return &UserRepository{
		db: db,
	}
}
