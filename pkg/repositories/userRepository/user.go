package userRepository

import (
	"context"
	"database/sql"
	"errors"
	"log"

	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
)

func (u UserRepository) CreateUserAccount(ctx context.Context, email string, password string) error {
	query := "INSERT INTO `user` (`email`, `password`) VALUES (?, ?)"
	sqlStatement, err := u.db.Prepare(query)
	if err != nil {
		log.Println(err)
		return err
	}
	defer sqlStatement.Close()

	_, err = sqlStatement.Exec(email, password)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (u UserRepository) GetUserAccountByID(ctx context.Context, userID int) (userData model.User, err error) {

	query := "select email, name, telephone, address from user where id=?"
	sqlStatement, err := u.db.Prepare(query)
	if err != nil {
		log.Println(err)
		return userData, err
	}
	defer sqlStatement.Close()

	rows, err := sqlStatement.Query(userID)
	if err != nil {
		log.Panicln(err)
		return userData, err
	}
	defer rows.Close()

	if !rows.Next() {
		return userData, errors.New("data not found")
	}

	var name, telephone, address sql.NullString

	err = rows.Scan(&userData.Email, &name, &telephone, &address)
	if err != nil {
		log.Println(err)
		return userData, err
	}

	userData.Address = address.String
	userData.Name = name.String
	userData.Telephone = telephone.String

	return userData, nil
}

func (u UserRepository) GetUserAccountByEmail(ctx context.Context, email string) (userData model.User, err error) {
	query := "select id, email, name, telephone, address, password from user where email=?"
	sqlStatement, err := u.db.Prepare(query)
	if err != nil {
		log.Println(err)
		return userData, err
	}
	defer sqlStatement.Close()

	rows, err := sqlStatement.Query(email)
	if err != nil {
		log.Panicln(err)
		return userData, err
	}
	defer rows.Close()

	if !rows.Next() {
		return userData, sql.ErrNoRows
	}

	var name, telephone, address, password sql.NullString

	err = rows.Scan(&userData.ID, &userData.Email, &name, &telephone, &address, &password)
	if err != nil {
		log.Println(err)
		return userData, err
	}

	userData.Address = name.String
	userData.Telephone = telephone.String
	userData.Name = name.String
	userData.Password = password.String

	return userData, nil
}

func (u UserRepository) UpdateUserAccount(ctx context.Context, profile model.UserProfile, userID int) error {

	statement, err := u.db.Prepare("UPDATE `user` SET `email`=?, `name`=?, `telephone`=?, `address`=? WHERE `id`=?")
	if err != nil {
		log.Println(err)
		return err
	}
	defer statement.Close()

	_, err = statement.Exec(profile.Email, profile.Name, profile.Telephone, profile.Address, userID)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (u UserRepository) UpdateUserPassword(ctx context.Context, password string, userID int) error {

	statement, err := u.db.Prepare("UPDATE `user` SET `password`=? WHERE `id`=?")
	if err != nil {
		log.Println(err)
		return err
	}
	defer statement.Close()

	_, err = statement.Exec(password, userID)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
