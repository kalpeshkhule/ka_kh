package authRepository

import "database/sql"

type AuthRepository struct {
	db *sql.DB
}

func GetAuthRepository(db *sql.DB) *AuthRepository {
	return &AuthRepository{db}
}
