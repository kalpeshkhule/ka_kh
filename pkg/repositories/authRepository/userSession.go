package authRepository

import (
	"context"
	"log"
)

func (u AuthRepository) CreateUserSession(ctx context.Context, userID int, sessionID string) error {

	statement, err := u.db.Prepare("INSERT INTO `user_session` (`session_id`, `user_id`) VALUES (?, ?)")
	if err != nil {
		log.Println(err)
		return err
	}
	defer statement.Close()

	_, err = statement.Exec(sessionID, userID)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (u AuthRepository) DeleteUserSession(ctx context.Context, sessionID string) error {

	statement, err := u.db.Prepare("UPDATE `user_session` SET `is_valid`='0' WHERE session_id=?")
	if err != nil {
		log.Println(err)
		return err
	}
	defer statement.Close()

	_, err = statement.Exec(sessionID)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}
