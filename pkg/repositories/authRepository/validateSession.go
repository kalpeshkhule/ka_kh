package authRepository

import (
	"context"
	"database/sql"
	"log"

	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
)

func (u AuthRepository) GetUserSession(ctx context.Context, sessionID string) (us model.UserSession, err error) {

	statement, err := u.db.Prepare("select user_id, session_id, is_valid from user_session where session_id=?")
	if err != nil {
		log.Println(err)
		return us, err
	}
	defer statement.Close()

	rows, err := statement.Query(sessionID)
	if err != nil {
		log.Println(err)
		return us, err
	}
	defer rows.Close()

	if !rows.Next() {
		return us, sql.ErrNoRows
	}

	err = rows.Scan(&us.UserID, &us.SessionID, &us.IsValid)
	if err != nil {
		log.Println(err)
		return us, err
	}

	return us, nil
}
