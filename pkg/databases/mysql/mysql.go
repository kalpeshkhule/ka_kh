package mysql

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func GetConnectionPool(dbUser, dbPassword, dbHost, dbPort, dbName string) (*sql.DB, error) {

	dbConnectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbUser, dbPassword, dbHost, dbPort, dbName)

	db, err := sql.Open("mysql", dbConnectionString)
	if err != nil {
		log.Println("error connecting to database: ", err)
		return nil, err
	}

	return db, nil
}
