package service

import (
	"context"
	"fmt"
	"log"

	apierror "bitbucket.org/kalpeshkhule/ka_kh/pkg/apiError"
	"github.com/google/uuid"
)

func (u UserService) ForgotPassword(ctx context.Context, email string) error {

	user, err := u.userRepository.GetUserAccountByEmail(ctx, email)
	if err != nil {
		log.Println(err)
		return apierror.ErrInternalServer
	}

	otp := uuid.NewString()

	err = u.userRepository.AddOtp(ctx, otp, user.ID)
	if err != nil {
		return apierror.ErrInternalServer
	}

	passwordResetURL := fmt.Sprintf("%s/password/reset?email=%s&otp=%s", u.config.baseURL, email, otp)

	u.emailServer.SendForgotPasswordEmail(ctx, email, passwordResetURL)
	return nil
}

func (u UserService) ResetPassword(ctx context.Context, password string, otp string, email string) error {

	user, err := u.userRepository.GetUserAccountByEmail(ctx, email)
	if err != nil {
		log.Println(err)
		return apierror.ErrInternalServer
	}

	err = u.userRepository.ValidateOtp(ctx, user.ID, otp)
	if err != nil {
		return apierror.ErrNotAuthorized
	}

	password, err = hashPassword(ctx, password)
	if err != nil {
		log.Println(err)
		return apierror.ErrInternalServer
	}

	err = u.userRepository.UpdateUserPassword(ctx, password, user.ID)
	if err != nil {
		return apierror.ErrInternalServer
	}

	return nil
}
