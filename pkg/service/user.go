package service

import (
	"context"
	"database/sql"
	"log"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"

	apierror "bitbucket.org/kalpeshkhule/ka_kh/pkg/apiError"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
)

func (u UserService) UserSignUp(ctx context.Context, user model.UserLoginRequest) (res model.UserLoginResponse, err error) {

	password, err := hashPassword(ctx, user.Password)
	if err != nil {
		log.Println(err)
		return res, apierror.ErrInternalServer
	}

	userData, err := u.userRepository.GetUserAccountByEmail(ctx, user.Email)
	if err != nil && err != sql.ErrNoRows {
		return res, apierror.ErrInternalServer
	} else if err == sql.ErrNoRows {
		// if account is not found for the email, create the account
		err = u.userRepository.CreateUserAccount(ctx, user.Email, password)
		if err != nil {
			return res, apierror.ErrInternalServer
		}
	} else if userData.Password == "" {
		// if account is found, but it is account created with google oauth
		err = u.userRepository.UpdateUserPassword(ctx, password, userData.ID)
		if err != nil {
			return res, apierror.ErrInternalServer
		}
	} else {
		return res, apierror.ErrDuplicate
	}

	// if no record found (above line) or
	// if user's account is registered with gmail (password is blank)

	userData, err = u.userRepository.GetUserAccountByEmail(ctx, user.Email)
	if err != nil {
		return res, apierror.ErrInternalServer
	}

	sessionID := uuid.NewString()
	err = u.authRepository.CreateUserSession(ctx, userData.ID, sessionID)
	if err != nil {
		return res, apierror.ErrInternalServer
	}

	return model.UserLoginResponse{
		SessionID: sessionID,
		UserProfile: model.UserProfile{
			Email: user.Email,
		},
	}, nil
}

func (u UserService) UserLogin(ctx context.Context, req model.UserLoginRequest) (resp model.UserLoginResponse, err error) {

	user, err := u.userRepository.GetUserAccountByEmail(ctx, req.Email)
	if err != nil {
		return resp, apierror.ErrInternalServer
	}

	matched := checkPasswordHash(ctx, req.Password, user.Password)
	if !matched {
		return resp, apierror.ErrNotAuthorized
	}

	//if user's password matches with the one provided in the login, register user session
	sessionID := uuid.New().String()

	err = u.authRepository.CreateUserSession(ctx, user.ID, sessionID)
	if err != nil {
		log.Println(err)
		return resp, apierror.ErrInternalServer
	}

	resp.SessionID = sessionID
	resp.UserProfile = model.UserProfile{
		Name:      user.Name,
		Email:     user.Email,
		Address:   user.Address,
		Telephone: user.Telephone,
	}

	return resp, nil
}

func (u UserService) UserLogout(ctx context.Context, sessionID string) error {

	err := u.authRepository.DeleteUserSession(ctx, sessionID)
	if err != nil {
		return apierror.ErrInternalServer
	}

	return nil
}

func hashPassword(ctx context.Context, password string) (string, error) {

	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		log.Println(err)
		return "", err
	}

	return string(bytes), nil
}

func checkPasswordHash(ctx context.Context, password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
