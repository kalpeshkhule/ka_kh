package service

import (
	email "bitbucket.org/kalpeshkhule/ka_kh/pkg/emailServer"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/googleAuth"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/repositories/authRepository"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/repositories/userRepository"
)

type UserService struct {
	config         UserServiceConfig
	userRepository *userRepository.UserRepository
	authRepository *authRepository.AuthRepository
	gAuth          *googleAuth.GoogleAuth
	emailServer    email.EmailServer
}

type UserServiceConfig struct {
	baseURL string
}

func GetUserServiceConfig(baseURL string) UserServiceConfig {
	return UserServiceConfig{
		baseURL: baseURL,
	}
}

func GetUserService(u *userRepository.UserRepository, a *authRepository.AuthRepository,
	gAuth *googleAuth.GoogleAuth, e email.EmailServer, conf UserServiceConfig) *UserService {
	return &UserService{
		config:         conf,
		userRepository: u,
		authRepository: a,
		gAuth:          gAuth,
		emailServer:    e,
	}
}
