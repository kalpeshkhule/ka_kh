package service

import (
	"context"
	"database/sql"

	apierror "bitbucket.org/kalpeshkhule/ka_kh/pkg/apiError"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
)

func (u UserService) GetProfile(ctx context.Context, sessionID string) (profile model.UserProfile, err error) {
	userSession, err := u.authRepository.GetUserSession(ctx, sessionID)
	if err != nil {
		return profile, apierror.ErrInternalServer
	}

	user, err := u.userRepository.GetUserAccountByID(ctx, userSession.UserID)
	if err != nil {
		return profile, apierror.ErrInternalServer
	}

	profile.Name = user.Name
	profile.Address = user.Address
	profile.Telephone = user.Telephone
	profile.Email = user.Email

	return profile, nil
}

func (u UserService) EditProfile(ctx context.Context, profile model.UserProfile, sessionID string) (err error) {
	userSession, err := u.authRepository.GetUserSession(ctx, sessionID)
	if err != nil {
		return apierror.ErrInternalServer
	}

	user, err := u.userRepository.GetUserAccountByID(ctx, userSession.UserID)
	if err != nil {
		return apierror.ErrInternalServer
	}

	// check if new email already exists in the system or not
	if user.Email != profile.Email {
		_, err = u.userRepository.GetUserAccountByEmail(ctx, profile.Email)
		if err != nil && err != sql.ErrNoRows {
			return apierror.ErrInternalServer
		} else if err == nil {
			return apierror.ErrDuplicate
		}
	}

	err = u.userRepository.UpdateUserAccount(ctx, profile, userSession.UserID)
	if err != nil {
		return apierror.ErrInternalServer
	}

	return nil
}
