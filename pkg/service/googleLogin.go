package service

import (
	"context"
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	apierror "bitbucket.org/kalpeshkhule/ka_kh/pkg/apiError"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
	"github.com/google/uuid"
)

func (u UserService) GoogleLogin(ctx context.Context, accessCode string) (resp model.UserLoginResponse, err error) {

	token, err := u.gAuth.Gauth.Exchange(ctx, accessCode)
	if err != nil {
		log.Println(err)
		return resp, apierror.ErrInternalServer
	}

	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		log.Println(err)
		return resp, apierror.ErrInternalServer

	}
	defer response.Body.Close()

	var googleUserResponse model.GoogleAPIResponse
	json.NewDecoder(response.Body).Decode(&googleUserResponse)

	//verify if the user we got exists or not
	user, err := u.userRepository.GetUserAccountByEmail(ctx, googleUserResponse.Email)
	if err != nil && err == sql.ErrNoRows {
		// if user does not exist, create user's account
		err := u.userRepository.CreateUserAccount(ctx, googleUserResponse.Email, "")
		if err != nil {
			return resp, apierror.ErrInternalServer
		}
		user, err = u.userRepository.GetUserAccountByEmail(ctx, googleUserResponse.Email)
		if err != nil {
			return resp, apierror.ErrInternalServer
		}
	} else if err != nil {
		log.Println(err)
		return resp, apierror.ErrInternalServer
	}

	//if user's password matches with the one provided in the login, register user session
	sessionID := uuid.New().String()

	err = u.authRepository.CreateUserSession(ctx, user.ID, sessionID)
	if err != nil {
		log.Println(err)
		return resp, apierror.ErrInternalServer
	}

	resp.SessionID = sessionID
	resp.UserProfile = model.UserProfile{
		Name:      user.Name,
		Email:     user.Email,
		Address:   user.Address,
		Telephone: user.Telephone,
	}

	return resp, nil
}
