package webRoutes

import (
	"log"
	"net/http"

	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
	"github.com/go-playground/validator/v10"
)

func (wr WebRoutes) signUp(w http.ResponseWriter, r *http.Request) {

	// if it is a GET method call
	if r.Method == http.MethodGet {
		wr.template.ExecuteTemplate(w, "sign-up.html", nil)
		return
	}

	// if it is a POST method call
	loginRequest := model.UserLoginRequest{}

	loginRequest.Email = r.FormValue("email")
	loginRequest.Password = r.FormValue("password")

	err := validator.New().Struct(loginRequest)
	if err != nil {
		log.Println(err)
		data := model.WebErrorModel{
			ErrorMessage:     "kindly enter the valid data",
			ShowErrorMessage: true,
		}
		wr.template.ExecuteTemplate(w, "sign-up.html", data)
		return
	}

	userResponse := model.UserLoginResponse{}
	statusCode, err := wr.httpClient.Post("/v1/signup", loginRequest, r.Cookies(), &userResponse)
	if err != nil || statusCode != http.StatusOK {
		data := model.WebErrorModel{
			ShowErrorMessage: true,
		}

		switch statusCode {
		case http.StatusBadRequest:
			data.ErrorMessage = "Kindly check email and password format."
		case http.StatusConflict:
			data.ErrorMessage = "Email id already registered. Login instead."
		default:
			data.ErrorMessage = "Internal server error."
		}

		wr.template.ExecuteTemplate(w, "sign-up.html", data)
		return
	}

	cookie := &http.Cookie{
		Name:  "session_id",
		Value: userResponse.SessionID,
		Path:  "/",
	}
	http.SetCookie(w, cookie)

	//at the time of login, we have to redirect to profile before cookie is set in browser
	r.AddCookie(cookie)
	http.Redirect(w, r, "/profile", http.StatusFound)
}
