package webRoutes

import (
	"log"
	"net/http"
)

func (wr WebRoutes) logout(w http.ResponseWriter, r *http.Request) {

	_, err := wr.httpClient.Get("/v1/logout", r.Cookies(), nil)
	if err != nil {
		log.Println("error invaidating session on server")
	}

	//deleting the cookie
	http.SetCookie(w, &http.Cookie{
		Name:   "session_id",
		MaxAge: -1,
	})

	http.Redirect(w, r, "/login", http.StatusFound)
}
