package webRoutes

import (
	"html/template"

	"bitbucket.org/kalpeshkhule/ka_kh/pkg/googleAuth"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/middleware"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/webRoutes/httpClient"
	"github.com/gorilla/mux"
)

type WebRoutes struct {
	template   *template.Template
	gAuth      *googleAuth.GoogleAuth
	httpClient *httpClient.HttpClient
}

func GetWebRoutes(t *template.Template, g *googleAuth.GoogleAuth, h *httpClient.HttpClient) *WebRoutes {
	return &WebRoutes{
		template:   t,
		gAuth:      g,
		httpClient: h,
	}
}

func (w WebRoutes) InitWebRoutes(r *mux.Router, auth middleware.AuthMiddleware) {

	authRoutes := r.NewRoute().Subrouter()
	w.getAuthRoutes(authRoutes)
	authRoutes.Use(auth.AuthMiddlewareFunc)

	nonAuthRoutes := r.NewRoute().Subrouter()
	w.getNonAuthRoutes(nonAuthRoutes)
}

func (w WebRoutes) getNonAuthRoutes(r *mux.Router) {
	r.HandleFunc("/", w.login).Methods("GET")
	r.HandleFunc("/login", w.login).Methods("GET", "POST")
	r.HandleFunc("/sign-up", w.signUp).Methods("GET", "POST")
	r.HandleFunc("/google-auth", w.googleAuth).Methods("GET")
	r.HandleFunc("/callback", w.callback).Methods("GET")

	r.HandleFunc("/password/forgot", w.forgotPassword).Methods("GET", "POST")
	r.HandleFunc("/password/reset", w.resetPassword).Methods("GET", "POST")
}

func (w WebRoutes) getAuthRoutes(r *mux.Router) {
	r.HandleFunc("/profile", w.profile).Methods("GET")
	r.HandleFunc("/profile/edit", w.editProfile).Methods("GET", "POST")
	r.HandleFunc("/logout", w.logout).Methods("GET")

}
