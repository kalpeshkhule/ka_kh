package httpClient

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type HttpClient struct {
	apiBaseURL string
	client     *http.Client
}

func GetHttpClient(baseURL string) *HttpClient {

	client := &http.Client{
		Timeout: 30 * time.Second,
	}
	return &HttpClient{
		apiBaseURL: baseURL,
		client:     client,
	}
}

func ReadResponse(resp *http.Response, v interface{}) error {

	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return err
	}

	err = json.Unmarshal(bodyBytes, v)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}
