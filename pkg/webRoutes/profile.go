package webRoutes

import (
	"net/http"

	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
)

func (wr WebRoutes) profile(w http.ResponseWriter, r *http.Request) {

	profile := model.UserProfile{}
	statusCode, err := wr.httpClient.Get("/v1/profile", r.Cookies(), &profile)
	if err != nil || statusCode != http.StatusOK {
		data := model.WebErrorModel{
			ShowErrorMessage: true,
		}

		switch statusCode {
		case http.StatusUnauthorized:
			data.ErrorMessage = "user not authorized, please login again"
		default:
			data.ErrorMessage = "could not load profile"
		}

		wr.template.ExecuteTemplate(w, "nonAuthorizedUser.html", data)
		return
	}

	wr.template.ExecuteTemplate(w, "profile.html", profile)
}

func (wr WebRoutes) editProfile(w http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodGet {
		profile := model.UserProfile{}
		statusCode, err := wr.httpClient.Get("/v1/profile", r.Cookies(), &profile)
		if err != nil || statusCode != http.StatusOK {
			data := model.WebErrorModel{
				ShowErrorMessage: true,
			}

			switch statusCode {
			case http.StatusUnauthorized:
				data.ErrorMessage = "user not authorized, please login again"
				wr.template.ExecuteTemplate(w, "nonAuthorizedUser.html", data)
				return
			default:
				data.ErrorMessage = "could not load profile, please retry"
			}

			wr.template.ExecuteTemplate(w, "editProfile.html", data)
			return
		}

		wr.template.ExecuteTemplate(w, "editProfile.html", profile)
		return
	}

	if r.Method == http.MethodPost {
		var userProfile model.UserProfile
		userProfile.Email = r.FormValue("email")
		userProfile.Name = r.FormValue("name")
		userProfile.Address = r.FormValue("address")
		userProfile.Telephone = r.FormValue("telephone")

		statusCode, err := wr.httpClient.Post("/v1/profile/edit", userProfile, r.Cookies(), nil)
		if err != nil || statusCode != http.StatusOK {
			userProfile.ShowErrorMessage = true
			switch statusCode {
			case http.StatusConflict:
				userProfile.ErrorMessage = "email already exist in the system"
			case http.StatusUnauthorized:
				userProfile.ErrorMessage = "user not authorized, please login again"
			default:
				userProfile.ErrorMessage = "could not update profile"
			}
			wr.template.ExecuteTemplate(w, "editProfile.html", userProfile)
			return
		}

		http.Redirect(w, r, "/profile", http.StatusFound)
		return
	}
}
