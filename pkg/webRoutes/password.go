package webRoutes

import (
	"net/http"

	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
	"github.com/go-playground/validator/v10"
)

func (wr WebRoutes) forgotPassword(w http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodGet {
		wr.template.ExecuteTemplate(w, "forgot-password.html", nil)
		return
	}

	if r.Method == http.MethodPost {

		email := r.FormValue("email")

		req := model.ForgetPasswordRequest{
			Email: email,
		}

		err := validator.New().Struct(req)
		if err != nil {
			data := model.WebErrorModel{
				ErrorMessage:     "please enter valid email address",
				ShowErrorMessage: true,
			}
			wr.template.ExecuteTemplate(w, "forgot-password.html", data)
			return
		}

		_, err = wr.httpClient.Post("/v1/password/forgot", req, r.Cookies(), nil)
		if err != nil {
			data := model.WebErrorModel{
				ErrorMessage:     "could not send password reset email, please try again",
				ShowErrorMessage: true,
			}
			wr.template.ExecuteTemplate(w, "forgot-password.html", data)
			return
		}

		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
}

func (wr WebRoutes) resetPassword(w http.ResponseWriter, r *http.Request) {

	var req model.ResetPasswordRequest
	if r.Method == http.MethodGet {

		req.Otp = r.FormValue("otp")
		req.Email = r.FormValue("email")
		wr.template.ExecuteTemplate(w, "reset-password.html", req)
		return
	}

	if r.Method == http.MethodPost {
		req.Otp = r.FormValue("otp")
		req.Email = r.FormValue("email")
		req.NewPassword = r.FormValue("new_password")
		req.ConfirmPassword = r.FormValue("confirm_password")

		statusCode, err := wr.httpClient.Post("/v1/password/reset", req, r.Cookies(), nil)
		if err != nil || statusCode != http.StatusOK {

			switch statusCode {
			case http.StatusUnauthorized:
				req.ErrorMessage = "Invalid username or password"
			case http.StatusBadRequest:
				req.ErrorMessage = "Either invalid email or passwords does not match"
			default:
				req.ErrorMessage = "Internal server error"
			}
			req.ShowErrorMessage = true
			wr.template.ExecuteTemplate(w, "reset-password.html", req)
			return
		}

		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
}
