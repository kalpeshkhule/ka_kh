package webRoutes

import (
	"net/http"

	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
	"github.com/go-playground/validator/v10"
)

func (wr WebRoutes) login(w http.ResponseWriter, r *http.Request) {

	// if it is a GET method call
	if r.Method == http.MethodGet {
		wr.template.ExecuteTemplate(w, "login.html", nil)
		return
	}

	// if it is a POST method call
	loginRequest := model.UserLoginRequest{}

	loginRequest.Email = r.FormValue("email")
	loginRequest.Password = r.FormValue("password")

	err := validator.New().Struct(loginRequest)
	if err != nil {
		data := model.WebErrorModel{
			ErrorMessage:     "kindly enter the valid data",
			ShowErrorMessage: true,
		}
		wr.template.ExecuteTemplate(w, "login.html", data)
		return
	}

	userResponse := model.UserLoginResponse{}
	statusCode, err := wr.httpClient.Post("/v1/login", loginRequest, r.Cookies(), &userResponse)
	if err != nil || statusCode != http.StatusOK {
		data := model.WebErrorModel{
			ShowErrorMessage: true,
		}

		switch statusCode {
		case http.StatusUnauthorized:
			data.ErrorMessage = "Invalid username or password"
		default:
			data.ErrorMessage = "Internal server error"
		}

		wr.template.ExecuteTemplate(w, "login.html", data)
		return
	}

	// on successful API call
	cookie := &http.Cookie{
		Name:  "session_id",
		Value: userResponse.SessionID,
		Path:  "/",
	}
	http.SetCookie(w, cookie)

	//at the time of login, we have to redirect to profile before cookie is set in browser
	r.AddCookie(cookie)

	http.Redirect(w, r, "/profile", http.StatusFound)
}
