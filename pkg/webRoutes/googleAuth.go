package webRoutes

import (
	"net/http"

	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
)

func (wr WebRoutes) googleAuth(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, wr.gAuth.Gauth.AuthCodeURL(wr.gAuth.State), http.StatusFound)
}

func (wr WebRoutes) callback(w http.ResponseWriter, r *http.Request) {

	state := r.FormValue("state")
	code := r.FormValue("code")

	if state != wr.gAuth.State || code == "" {
		errData := model.WebErrorModel{
			ShowErrorMessage: true,
			ErrorMessage:     "could not verify credentials, try again",
		}
		wr.template.ExecuteTemplate(w, "nonAuthorizedUser.html", errData)
		return
	}

	userResponse := model.UserLoginResponse{}

	statusCode, err := wr.httpClient.Post("/v1/login/google", model.GoogleLoginRequest{
		Code: code,
	}, r.Cookies(), &userResponse)
	if err != nil || statusCode != http.StatusOK {
		if err != nil {
			errData := model.WebErrorModel{
				ShowErrorMessage: true,
				ErrorMessage:     "could not verify credentials, try again",
			}
			wr.template.ExecuteTemplate(w, "nonAuthorizedUser.html", errData)
			return
		}
	}

	cookie := &http.Cookie{
		Name:  "session_id",
		Value: userResponse.SessionID,
		Path:  "/",
	}
	http.SetCookie(w, cookie)

	//at the time of login, we have to redirect to profile before cookie is set in browser
	r.AddCookie(cookie)

	http.Redirect(w, r, "/profile", http.StatusFound)

}
