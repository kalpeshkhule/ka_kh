package googleAuth

import (
	"github.com/google/uuid"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

type GoogleAuth struct {
	State string
	Gauth *oauth2.Config
}

func GetGoogleAuth(redirect string, googleCID string, googleSecret string) *GoogleAuth {

	return &GoogleAuth{
		State: uuid.NewString(),
		Gauth: &oauth2.Config{
			RedirectURL:  redirect,
			ClientID:     googleCID,
			ClientSecret: googleSecret,
			Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
			Endpoint:     google.Endpoint,
		},
	}
}
