package emailServer

import (
	"bytes"
	"context"
	"html/template"
	"log"
	"net/smtp"
)

type EmailServer struct {
	serverAuth  smtp.Auth
	hostAddress string
	senderEmail string
	templates   *template.Template
}

func GetEmailServerInstance(user string, password string, host string, port string, template *template.Template) EmailServer {
	auth := smtp.PlainAuth("", user, password, host)
	return EmailServer{
		serverAuth:  auth,
		hostAddress: host + ":" + port,
		senderEmail: user,
		templates:   template,
	}
}

func (e EmailServer) SendForgotPasswordEmail(ctx context.Context, to string, link string) error {

	log.Println("sending reset password email to: ", to)
	subject := "Password reset link"
	body, err := e.parseTemplate(ctx, "reset-password-email.html", template.URL(link))
	if err != nil {
		log.Println(err)
		return err
	}

	err = e.sendEmail(ctx, []string{to}, subject, body)
	if err != nil {
		log.Println(err)
		return err
	}

	log.Println("reset email sent successfully to: ", to)
	return nil
}

func (e EmailServer) sendEmail(ctx context.Context, to []string, subject string, body string) error {
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	sub := "Subject: " + subject + "!\n"
	msg := []byte(sub + mime + "\n" + body)

	err := smtp.SendMail(e.hostAddress, e.serverAuth, e.senderEmail, to, msg)
	if err != nil {
		return err
	}
	return nil
}

func (e EmailServer) parseTemplate(ctx context.Context, templateFileName string, data interface{}) (string, error) {

	buf := new(bytes.Buffer)
	err := e.templates.ExecuteTemplate(buf, templateFileName, data)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}
