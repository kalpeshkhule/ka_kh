package model

type User struct {
	ID        int
	Name      string
	Email     string
	Telephone string
	Address   string
	Password  string
}

type UserSession struct {
	UserID    int
	SessionID string
	IsValid   bool
}
