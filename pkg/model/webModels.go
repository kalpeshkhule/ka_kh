package model

type WebErrorModel struct {
	ErrorMessage     string
	ShowErrorMessage bool
}

type UserLoginRequest struct {
	Email    string `json:"email" validate:"email,required"`
	Password string `json:"password" validate:"min=6,max=20,required"`
}

type UserLoginResponse struct {
	SessionID string `json:"session_id"`
	UserProfile
}

type UserProfile struct {
	Name      string `json:"name" validate:""`
	Email     string `json:"email" validate:"email"`
	Address   string `json:"address" validate:""`
	Telephone string `json:"telephone" validate:""`
	WebErrorModel
}

type ForgetPasswordRequest struct {
	Email string `json:"email" validate:"email,required"`
}

type ResetPasswordRequest struct {
	Email           string `json:"email" validate:"email,required"`
	NewPassword     string `json:"new_password" validate:"min=6,max=20,required"`
	ConfirmPassword string `json:"confirm_password" validate:"min=6,max=20,required"`
	Otp             string `json:"otp" validate:"required"`
	WebErrorModel
}
