package model

type GoogleAPIResponse struct {
	ID       string `json:"id"`
	Email    string `json:"email"`
	Verified bool   `json:"verified_email"`
}

type GoogleLoginRequest struct {
	Code string `json:"access_code" validate:"required"`
}
