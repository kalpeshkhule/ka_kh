package apierror

import "net/http"

type ApiError struct {
	HttpStatusCode int
	ErrorMessage   string
}

func (a ApiError) Error() string {
	return a.ErrorMessage
}

var (
	ErrInternalServer = ApiError{HttpStatusCode: 500, ErrorMessage: "internal server error"}
	ErrNotAuthorized  = ApiError{HttpStatusCode: http.StatusUnauthorized, ErrorMessage: "request not authorized"}
	ErrDuplicate      = ApiError{HttpStatusCode: http.StatusConflict, ErrorMessage: "duplicate resource"}
)
