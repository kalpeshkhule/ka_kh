package middleware

import (
	"context"
	"html/template"
	"log"
	"net/http"
	"strings"

	"bitbucket.org/kalpeshkhule/ka_kh/pkg/repositories/authRepository"
)

type AuthMiddleware struct {
	template       *template.Template
	authRepository *authRepository.AuthRepository
}

func GetAuthMiddleware(t *template.Template, a *authRepository.AuthRepository) AuthMiddleware {
	return AuthMiddleware{
		template:       t,
		authRepository: a,
	}
}

func (a AuthMiddleware) AuthMiddlewareFunc(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		log.Println("serving:", r.RequestURI)
		if strings.Contains(r.RequestURI, "/api/") {
			sessionID := r.Header.Get("session_id")
			if !a.validSession(sessionID) {
				http.Error(w, "unauthorized user access", http.StatusUnauthorized)
				return
			}
		} else {
			cookie, err := r.Cookie("session_id")
			if err == http.ErrNoCookie || !a.validSession(cookie.Value) {
				http.SetCookie(w, &http.Cookie{
					Name:   "session_id",
					MaxAge: -1,
				})
				a.template.ExecuteTemplate(w, "nonAuthorizedUser.html", nil)
				return
			}
		}

		// adding headers to authenticated routes for not maintaining cache
		w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
		w.Header().Set("Pragma", "no-cache")
		w.Header().Set("Expires", "0")

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}

func (a AuthMiddleware) validSession(sessionID string) bool {

	if sessionID == "" {
		return false
	}
	userSession, err := a.authRepository.GetUserSession(context.Background(), sessionID)
	if err != nil {
		return false
	}

	return userSession.IsValid
}
