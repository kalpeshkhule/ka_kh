package apiRoutes

import (
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/middleware"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/service"
	"github.com/gorilla/mux"
)

type APIRouter struct {
	userService *service.UserService
}

func GetAPIRouter(u *service.UserService) *APIRouter {
	return &APIRouter{
		userService: u,
	}
}

func (a APIRouter) InitAPIRoutes(r *mux.Router, auth middleware.AuthMiddleware) {

	authRoutes := r.NewRoute().Subrouter()
	a.getAuthRoutes(authRoutes)
	authRoutes.Use(middleware.LoggingMiddleware)
	authRoutes.Use(auth.AuthMiddlewareFunc)

	nonAuthRoutes := r.NewRoute().Subrouter()
	a.getNonAuthRoutes(nonAuthRoutes)
	nonAuthRoutes.Use(middleware.LoggingMiddleware)
}

func (a APIRouter) getNonAuthRoutes(r *mux.Router) {
	r.HandleFunc("/v1/login", a.login).Methods("POST")
	r.HandleFunc("/v1/signup", a.signUp).Methods("POST")
	r.HandleFunc("/v1/login/google", a.loginWithGoogle).Methods("POST")

	r.HandleFunc("/v1/password/forgot", a.forgotPassword).Methods("POST")
	r.HandleFunc("/v1/password/reset", a.resetPassword).Methods("POST")
}

func (a APIRouter) getAuthRoutes(r *mux.Router) {
	r.HandleFunc("/v1/profile", a.getProfile).Methods("GET")
	r.HandleFunc("/v1/profile/edit", a.editProfile).Methods("POST")
	r.HandleFunc("/v1/logout", a.logout).Methods("GET")
}
