package apiRoutes

import (
	"encoding/json"
	"log"
	"net/http"

	apierror "bitbucket.org/kalpeshkhule/ka_kh/pkg/apiError"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
	"github.com/go-playground/validator/v10"
)

func (a APIRouter) getProfile(w http.ResponseWriter, r *http.Request) {

	ctx := r.Context()
	sessionID := r.Header.Get("session_id")

	profile, err := a.userService.GetProfile(ctx, sessionID)
	if err != nil {
		http.Error(w, "could not fetch profile", http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(&profile)
	if err != nil {
		log.Println(err)
		http.Error(w, "error in encoding jason", http.StatusInternalServerError)
		return
	}
}

func (a APIRouter) editProfile(w http.ResponseWriter, r *http.Request) {

	ctx := r.Context()
	sessionID := r.Header.Get("session_id")
	profile := model.UserProfile{}

	err := json.NewDecoder(r.Body).Decode(&profile)
	if err != nil {
		log.Println(err)
		http.Error(w, "cannot read request body", http.StatusInternalServerError)
		return
	}

	err = validator.New().Struct(profile)
	if err != nil {
		log.Println(err)
		http.Error(w, "invalid request format", http.StatusBadRequest)
		return
	}

	err = a.userService.EditProfile(ctx, profile, sessionID)
	if err != nil {
		switch err.(apierror.ApiError) {
		case apierror.ErrDuplicate:
			http.Error(w, "updated email already exists in system, use different email", http.StatusConflict)
			return
		default:
			http.Error(w, "could not update profile", http.StatusInternalServerError)
			return
		}
	}
}
