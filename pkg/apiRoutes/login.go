package apiRoutes

import (
	"encoding/json"
	"log"
	"net/http"

	apierror "bitbucket.org/kalpeshkhule/ka_kh/pkg/apiError"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
	"github.com/go-playground/validator/v10"
)

func (a APIRouter) login(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	user := model.UserLoginRequest{}

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Println(err)
		http.Error(w, "cannot read request body", http.StatusInternalServerError)
		return
	}

	err = validator.New().Struct(user)
	if err != nil {
		log.Println(err)
		http.Error(w, "invalid request format", http.StatusBadRequest)
		return
	}

	resp, err := a.userService.UserLogin(ctx, user)
	if err != nil {
		switch err.(apierror.ApiError) {
		case apierror.ErrNotAuthorized:
			http.Error(w, "invalid username or password", http.StatusUnauthorized)
			return
		default:
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	err = json.NewEncoder(w).Encode(&resp)
	if err != nil {
		log.Println(err)
		http.Error(w, "error in encoding jason", http.StatusInternalServerError)
		return
	}
}

func (a APIRouter) loginWithGoogle(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	googleLoginRequest := model.GoogleLoginRequest{}

	err := json.NewDecoder(r.Body).Decode(&googleLoginRequest)
	if err != nil {
		log.Println(err)
		http.Error(w, "cannot read request body", http.StatusInternalServerError)
		return
	}

	err = validator.New().Struct(googleLoginRequest)
	if err != nil {
		log.Println(err)
		http.Error(w, "invalid request format", http.StatusBadRequest)
		return
	}

	resp, err := a.userService.GoogleLogin(ctx, googleLoginRequest.Code)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(&resp)
	if err != nil {
		log.Println(err)
		http.Error(w, "error in encoding json", http.StatusInternalServerError)
		return
	}
}

func (a APIRouter) logout(w http.ResponseWriter, r *http.Request) {

	ctx := r.Context()
	sessionID := r.Header.Get("session_id")

	err := a.userService.UserLogout(ctx, sessionID)
	if err != nil {
		http.Error(w, "could not logout", http.StatusInternalServerError)
		return
	}
}
