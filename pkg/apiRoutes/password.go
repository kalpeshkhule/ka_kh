package apiRoutes

import (
	"encoding/json"
	"log"
	"net/http"

	apierror "bitbucket.org/kalpeshkhule/ka_kh/pkg/apiError"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
	"github.com/go-playground/validator/v10"
)

func (a APIRouter) forgotPassword(w http.ResponseWriter, r *http.Request) {

	ctx := r.Context()
	req := model.ForgetPasswordRequest{}

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		log.Println(err)
		http.Error(w, "cannot read request body", http.StatusInternalServerError)
		return
	}

	err = validator.New().Struct(req)
	if err != nil {
		log.Println(err)
		http.Error(w, "invalid request format", http.StatusBadRequest)
		return
	}

	err = a.userService.ForgotPassword(ctx, req.Email)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (a APIRouter) resetPassword(w http.ResponseWriter, r *http.Request) {

	ctx := r.Context()
	req := model.ResetPasswordRequest{}

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		log.Println(err)
		http.Error(w, "cannot read request body", http.StatusInternalServerError)
		return
	}

	err = validator.New().Struct(req)
	if err != nil {
		log.Println(err)
		http.Error(w, "invalid request format", http.StatusBadRequest)
		return
	}

	if req.ConfirmPassword != req.NewPassword {
		http.Error(w, "new password and confirm password does not match", http.StatusBadRequest)
		return
	}

	err = a.userService.ResetPassword(ctx, req.NewPassword, req.Otp, req.Email)
	if err != nil {

		switch err.(apierror.ApiError) {
		case apierror.ErrNotAuthorized:
			http.Error(w, "invalid or expired otp", http.StatusUnauthorized)
			return
		default:
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}
