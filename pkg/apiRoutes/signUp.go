package apiRoutes

import (
	"encoding/json"
	"log"
	"net/http"

	apierror "bitbucket.org/kalpeshkhule/ka_kh/pkg/apiError"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/model"
	"github.com/go-playground/validator/v10"
)

func (a APIRouter) signUp(w http.ResponseWriter, r *http.Request) {

	ctx := r.Context()
	user := model.UserLoginRequest{}

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Println(err)
		http.Error(w, "cannot read request body", http.StatusInternalServerError)
		return
	}

	err = validator.New().Struct(user)
	if err != nil {
		log.Println(err)
		http.Error(w, "invalid request format", http.StatusBadRequest)
		return
	}

	userSignUpResponse, err := a.userService.UserSignUp(ctx, user)
	if err != nil {
		switch err.(apierror.ApiError) {
		case apierror.ErrDuplicate:
			http.Error(w, "Email already exists in system, use different email or try login", http.StatusConflict)
			return
		default:
			http.Error(w, "Internal server error, please try again later", http.StatusInternalServerError)
			return
		}
	}

	err = json.NewEncoder(w).Encode(&userSignUpResponse)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}
