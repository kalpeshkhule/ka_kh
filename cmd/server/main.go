package main

import (
	"database/sql"
	"html/template"
	"log"
	"net/http"
	"os"

	"bitbucket.org/kalpeshkhule/ka_kh/pkg/apiRoutes"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/databases/mysql"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/emailServer"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/googleAuth"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/middleware"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/repositories/authRepository"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/repositories/userRepository"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/service"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/webRoutes"
	"bitbucket.org/kalpeshkhule/ka_kh/pkg/webRoutes/httpClient"
	"github.com/gorilla/mux"
)

func main() {

	// initialize database connection
	db, err := getMySQLConnection()
	if err != nil {
		log.Println(err)
		panic("error connecting database")
	}

	// initialize templates to be used
	template := getTemplateInstance()

	// initialize google oauth config
	gAuth := getGoogleOauth()

	// initialize http client for web router
	httpClient := getHttpClient()

	// initialize database repositories
	userRepository := userRepository.GetUserRepository(db)
	authRepository := authRepository.GetAuthRepository(db)

	// initialize email server instance
	emailServer := getEmailServerInstance(template)

	// initialize services
	userServiceConfig := service.GetUserServiceConfig(os.Getenv("base_url"))
	userService := service.GetUserService(userRepository, authRepository, gAuth, emailServer, userServiceConfig)

	//initialize middlewares
	authMiddleware := middleware.GetAuthMiddleware(template, authRepository)

	//initialize routers
	r := mux.NewRouter()                                                    // initialize mux router
	initRoutes(r, authMiddleware, userService, template, gAuth, httpClient) // initialize web and api routes

	log.Println("starting the server")
	if err := http.ListenAndServe(":8080", r); err != nil {
		panic("cannot initialize server with error: " + err.Error())
	}

}

func getMySQLConnection() (*sql.DB, error) {

	dbUser := os.Getenv("db_user")
	dbPassword := os.Getenv("db_password")
	dbHost := os.Getenv("db_host")
	dbPort := os.Getenv("db_port")
	dbName := os.Getenv("db_name")
	return mysql.GetConnectionPool(dbUser, dbPassword, dbHost, dbPort, dbName)
}

func getTemplateInstance() *template.Template {
	log.Println("templates path: ", os.Getenv("template_path"))
	return template.Must(template.ParseGlob(os.Getenv("template_path") + "/*.html"))
}

func initRoutes(r *mux.Router, authMiddleware middleware.AuthMiddleware, u *service.UserService, t *template.Template, g *googleAuth.GoogleAuth, h *httpClient.HttpClient) {

	//initialize API router
	subRouter1 := r.PathPrefix("/api").Subrouter()
	apiRouter := apiRoutes.GetAPIRouter(u)
	apiRouter.InitAPIRoutes(subRouter1, authMiddleware)

	//initialize web router
	subRouter2 := r.PathPrefix("/").Subrouter()
	wr := webRoutes.GetWebRoutes(t, g, h)
	wr.InitWebRoutes(subRouter2, authMiddleware)

}

func getGoogleOauth() *googleAuth.GoogleAuth {
	redirectURL := os.Getenv("google_oauth_redirect_url")
	googleCID := os.Getenv("google_oauth_client_id")
	googleSecret := os.Getenv("google_oauth_secret")

	if redirectURL == "" || googleCID == "" || googleSecret == "" {
		panic("could not initialize google auth config")
	}

	return googleAuth.GetGoogleAuth(redirectURL, googleCID, googleSecret)
}

func getHttpClient() *httpClient.HttpClient {
	apiBaseURL := os.Getenv("api_base_url")
	if apiBaseURL == "" {
		log.Println("base url not found in environment variables")
		panic("base url not found")
	}

	return httpClient.GetHttpClient(apiBaseURL)
}

func getEmailServerInstance(t *template.Template) emailServer.EmailServer {

	smtpUser := os.Getenv("smtp_user")
	smtpPassword := os.Getenv("smtp_password")
	smtpHost := os.Getenv("smtp_host")
	smtpPort := os.Getenv("smtp_port")

	if smtpHost == "" || smtpPassword == "" || smtpPort == "" || smtpUser == "" {
		log.Println("did not get smtp credentials in environment variable")
		panic("did not get smtp credentials in environment variable")
	}
	return emailServer.GetEmailServerInstance(smtpUser, smtpPassword, smtpHost, smtpPort, t)
}
