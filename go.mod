module bitbucket.org/kalpeshkhule/ka_kh

go 1.15

require (
	github.com/go-playground/validator/v10 v10.10.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
)
